
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class RearrangeList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        rearrangeList();
        rearrangeList();
    }
    public static void rearrangeList(){
        ArrayList<String> al = new ArrayList<String>();
        al.add("Emelisa");
        al.add("Erwin");
        al.add("Elizabeth");
        al.add("Sophia");
        al.add("Lance");
  
        System.out.println("Original List : \n" + al);
  
        Collections.shuffle(al);
  
        System.out.println("\nShuffled List : \n" + al);
    }
    
}
