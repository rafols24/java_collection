
import java.util.HashSet;
import java.util.Set;







/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class Activity7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Compare();
        
    }
  public static void Compare(){
       Set<String> al = new HashSet<String>();
       al.add("first");
       al.add("second");
       al.add("third");
       al.add("apple");
       al.add("rat");
 
       Set<String> subSet = new HashSet<String>();

       subSet.add("rat");
       subSet.add("second");
       subSet.add("first");
       subSet.add("sixth");
 
       System.out.println("First Set:  " + al);
       System.out.println("Second Set: " + subSet);
       
       Set<String> firstResult = new HashSet<String>(al);
       firstResult.removeAll(subSet);
       Set<String> secondResult = new HashSet<String>(subSet);
       secondResult.removeAll(al);

      firstResult.addAll(secondResult);
      System.out.println("Different Values: " + firstResult);
  }
 }
    

