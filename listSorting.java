
import java.util.ArrayList;
import java.util.Collections;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author capstonestudent
 */
public class listSorting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        sortList();
    }
    public static void sortList(){
        ArrayList<String> al = new ArrayList<String>();
        al.add("Emelisa");
        al.add("Erwin");
        al.add("Elizabeth");
        al.add("Sophia");
        al.add("Lance");
        Collections.sort(al);
        System.out.println("List after the use of" +
                           " Collection.sort() :\n" + al);
    }
    
}
