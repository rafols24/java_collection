

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class isEmptyOversize {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Why should we opt for isEmpty() over size?");
        System.out.println("ANSWER:");
        System.out.println("We should choose isEmpty() rather than a size because with isEmpty we can read and maintain our code easier");
        System.out.println("Another reason is that when we use isEmpty the response rate would be faster than using a size because when we use size, as the elements increases it gets slower.");
        
    }
  
 }
    

