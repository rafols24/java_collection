

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 2ndyrGroupA
 */
public class Activity5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Compare and contrast the classic for loop versus foreach. What are the pros and cons of both sides?");
        System.out.println("ANSWER:");
        System.out.println("foreach is used to iterate over each element of a given set or list ");
        System.out.println("while for loop is used to access the object only by index");
        System.out.println("for loop is useful in a way that  we know exactly how many times the loop will execute before the loop starts however for us to make sure that it gets in the loop we need a thorough testing first");
        System.out.println("foreach loop is useful because when we will use this we can traverse each of our element one by one which eliminates the risks of bugs.");
        System.out.println("However foreach loop does not keep track on our indexes");
        
    }
  
 }
    

